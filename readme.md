Apidoc template
===

Fork do template https://github.com/interledger/apidoc-template.

# Como usar

**VIA git clone**

Clone o repositório para um diretório e informe o local com a opção `template`.

```shell
$ git clone git@bitbucket.org:phonetrack/apidoc-template.git
$ cd yourproject
$ apidoc --template ../apidoc-template
```

**VIA NPM**

Instale o pacote via NPM e informe o local com a opção `template`

```shell
$ npm install git+ssh://git@bitbucket.org:phonetrack/apidoc-template.git
$ cd yourproject
$ apidoc --template node_modules/apidoc-template
```

## Opções
Opções adicionais do API doc:

Atributo | Tipo | Descrição
-------- | ---- | ---------
menu | Array | Cria um menu na top-navitation do lado esquerdo.
menu[].url | String | Link do menu
menu[].label | String | Texto do link
showColorSwitch | Boolean | Habilita a troca de cores do tema.
repositoryUrl | String | URL do repositório de código.
logoUrl | String | URL da logo que aparece na top-navitation do lado esquerdo.
